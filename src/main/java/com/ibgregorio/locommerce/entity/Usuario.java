package com.ibgregorio.locommerce.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibgregorio.locommerce.entity.enums.AuthProviderEnum;
import com.ibgregorio.locommerce.entity.enums.PerfilUsuarioEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	private String email;
	
	@JsonIgnore
	private String senha;
	
	private String cpf;
	
	private String numeTel;
	
	private String numeCelular;
	
	private Date dtExclusao;
	
	@Enumerated(EnumType.STRING)
	private AuthProviderEnum provider;
	
	private String providerId;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "PERFIS")
	private Set<Integer> perfis = new HashSet<>();
	
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
	private List<Endereco> enderecos = new ArrayList<>();
	
	@OneToOne(mappedBy = "proprietario", cascade = CascadeType.ALL)
	private Loja loja;
	
	@JsonIgnore
	@Transient
	private Loja novaLoja;
	
	public Usuario(Long id, String nome, String email, String senha, String cpf, String numeTel, String numeCelular,
			Date dtExclusao, AuthProviderEnum provider, String providerId, Loja loja) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
		this.cpf = cpf;
		this.numeTel = numeTel;
		this.numeCelular = numeCelular;
		this.dtExclusao = dtExclusao;
		this.provider = provider;
		this.providerId = providerId;
		this.loja = loja;
	}
	
	public Set<PerfilUsuarioEnum> getPerfis() {
		return perfis.stream().map(x -> PerfilUsuarioEnum.toEnum(x)).collect(Collectors.toSet());
	}

	public void addPerfil(PerfilUsuarioEnum perfil) {
		perfis.add(perfil.getId());
	}

}
