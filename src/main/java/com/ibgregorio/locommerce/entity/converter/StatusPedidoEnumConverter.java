package com.ibgregorio.locommerce.entity.converter;

import javax.persistence.AttributeConverter;

import com.ibgregorio.locommerce.entity.enums.StatusPedidoEnum;

public class StatusPedidoEnumConverter implements AttributeConverter<StatusPedidoEnum, String> {

	@Override
	public String convertToDatabaseColumn(StatusPedidoEnum attribute) {
		
		if (attribute == null) {
			return null;
		}
		
		return attribute.getId();
	}

	@Override
	public StatusPedidoEnum convertToEntityAttribute(String dbData) {
		return StatusPedidoEnum.toEnum(dbData);
	}

}
