package com.ibgregorio.locommerce.entity.converter;

import javax.persistence.AttributeConverter;

import com.ibgregorio.locommerce.entity.enums.StatusEnum;

public class StatusEnumConverter implements AttributeConverter<StatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(StatusEnum attribute) {
		
		if (attribute == null) {
			return null;
		}
		
		return attribute.getId();
	}

	@Override
	public StatusEnum convertToEntityAttribute(String dbData) {
		return StatusEnum.toEnum(dbData);
	}

}
