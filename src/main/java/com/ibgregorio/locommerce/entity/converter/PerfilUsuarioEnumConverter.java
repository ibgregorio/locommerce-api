package com.ibgregorio.locommerce.entity.converter;

import javax.persistence.AttributeConverter;

import com.ibgregorio.locommerce.entity.enums.PerfilUsuarioEnum;

public class PerfilUsuarioEnumConverter implements AttributeConverter<PerfilUsuarioEnum, Integer> {

	@Override
	public Integer convertToDatabaseColumn(PerfilUsuarioEnum attribute) {
		
		if (attribute == null) {
			return null;
		}
		
		return attribute.getId();
	}

	@Override
	public PerfilUsuarioEnum convertToEntityAttribute(Integer dbData) {
		return PerfilUsuarioEnum.toEnum(dbData);
	}

}
