package com.ibgregorio.locommerce.entity.converter;

import javax.persistence.AttributeConverter;

import com.ibgregorio.locommerce.entity.enums.SimNaoEnum;

public class SimNaoEnumConverter implements AttributeConverter<SimNaoEnum, String> {

	@Override
	public String convertToDatabaseColumn(SimNaoEnum attribute) {
		
		if (attribute == null) {
			return null;
		}
		
		return attribute.getId();
	}

	@Override
	public SimNaoEnum convertToEntityAttribute(String dbData) {
		return SimNaoEnum.toEnum(dbData);
	}

}
