package com.ibgregorio.locommerce.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibgregorio.locommerce.entity.converter.StatusPedidoEnumConverter;
import com.ibgregorio.locommerce.entity.enums.StatusPedidoEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Pedido implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPedido;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAtualizacao;
	
	@Convert(converter = StatusPedidoEnumConverter.class)
	private StatusPedidoEnum status;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataPagamento;
	
	private String formaPgto;
	
	@ManyToOne
	@JoinColumn(name = "id_comprador")
	private Usuario comprador;
	
	@ManyToOne
	@JoinColumn(name = "id_endereco_entrega")
	private Endereco enderecoEntrega;

	@OneToMany(mappedBy = "id.pedido")
	private Set<PedidoItem> itensPedido = new HashSet<>();

	public Pedido(Long id, Date dataPedido, Date dataAtualizacao, StatusPedidoEnum status, Date dataPagamento,
			String formaPgto, Usuario comprador, Endereco enderecoEntrega) {
		super();
		this.id = id;
		this.dataPedido = dataPedido;
		this.dataAtualizacao = dataAtualizacao;
		this.status = status;
		this.dataPagamento = dataPagamento;
		this.formaPgto = formaPgto;
		this.comprador = comprador;
		this.enderecoEntrega = enderecoEntrega;
	}
	
	public double getValorTotal() {
		double soma = 0.0;
		for (PedidoItem ip : itensPedido) {
			soma += ip.getSubTotal(); 
		}
		
		return soma;
	}

}
