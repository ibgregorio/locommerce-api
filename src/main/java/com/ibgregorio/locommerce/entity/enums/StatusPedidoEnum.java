package com.ibgregorio.locommerce.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusPedidoEnum {

	PENDENTE("P", "Pendente"),
	APROVADO("A", "Aprovado"),
	CANCELADO("C", "Cancelado"),
	EMTRANSITO("T", "Em trânsito"),
	ENTREGUE("E", "Entregue");

	private String id;
	
	private String descricao;
	
	public static StatusPedidoEnum toEnum(String id) {
		if (id == null) {
			return null;
		}
		
		for (StatusPedidoEnum opcao : StatusPedidoEnum.values()) {
			if (id.equals(opcao.getId())) {
				return opcao;
			}
		}
		
		throw new IllegalArgumentException("ID inválido: " + id);
	}
}
