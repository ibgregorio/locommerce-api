package com.ibgregorio.locommerce.entity.enums;

import lombok.Getter;
import lombok.AllArgsConstructor;

@Getter
@AllArgsConstructor
public enum StatusEnum {
	
	ATIVO("A", "Ativo"),
	INATIVO("I", "Inativo");

	private String id;
	
	private String descricao;
	
	public static StatusEnum toEnum(String id) {
		if (id == null) {
			return null;
		}
		
		for (StatusEnum opcao : StatusEnum.values()) {
			if (id.equals(opcao.getId())) {
				return opcao;
			}
		}
		
		throw new IllegalArgumentException("ID inválido: " + id);
	}
}
