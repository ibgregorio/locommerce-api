package com.ibgregorio.locommerce.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PerfilUsuarioEnum {

	ADMIN(1, "ROLE_ADMIN"),
	VENDEDOR(2, "ROLE_VENDEDOR"),
	CLIENTE(3, "ROLE_CLIENTE");
	
	private Integer id;
	
	private String descricao;
	
	public static PerfilUsuarioEnum toEnum(Integer id) {
		if (id == null) {
			return null;
		}
		
		for (PerfilUsuarioEnum perfil : PerfilUsuarioEnum.values()) {
			if (id.equals(perfil.getId())) {
				return perfil;
			}
		}
		
		throw new IllegalArgumentException("ID inválido: " + id);
	}
}
