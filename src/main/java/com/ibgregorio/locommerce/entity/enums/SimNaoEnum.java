package com.ibgregorio.locommerce.entity.enums;

import lombok.Getter;
import lombok.AllArgsConstructor;

@Getter
@AllArgsConstructor
public enum SimNaoEnum {
	
	SIM("S", "Sim"),
	NAO("N", "Não");

	private String id;
	
	private String descricao;
	
	public static SimNaoEnum toEnum(String id) {
		if (id == null) {
			return null;
		}
		
		for (SimNaoEnum opcao : SimNaoEnum.values()) {
			if (id.equals(opcao.getId())) {
				return opcao;
			}
		}
		
		throw new IllegalArgumentException("ID inválido: " + id);
	}
}
