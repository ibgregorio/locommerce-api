package com.ibgregorio.locommerce.entity.enums;

public enum AuthProviderEnum {

	DEFAULT,
    GOOGLE
}
