package com.ibgregorio.locommerce.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class ItemDesejo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	@EmbeddedId
	private ItemDesejoPk id = new ItemDesejoPk();
	
	public ItemDesejo(ListaDesejo listaDesejo, Produto produto) {
		super();
		id.setListaDesejo(listaDesejo);
		id.setProduto(produto);
	}
	
	@JsonIgnore
	public ListaDesejo getListaDesejo() {
		return id.getListaDesejo();
	}
	
	public void setListaDesejo(ListaDesejo pedido) {
		id.setListaDesejo(pedido);
	}
	
	public Produto getProduto() {
		return id.getProduto();
	}
	
	public void setProduto(Produto produto) {
		id.setProduto(produto);
	}
}
