package com.ibgregorio.locommerce.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	private String detalhes;
	
	private Integer qtdEstoque;
	
	private Double preco;
	
	@OneToOne
	@JoinColumn(name = "id_categoria")
	private Categoria categoria;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "id_loja")
	private Loja loja;
	
	private Date dtAtualizacaoPreco;
	
	@JsonIgnore
	@OneToMany(mappedBy = "id.produto")
	private Set<PedidoItem> itens = new HashSet<>();

	public Produto(Long id, String nome, String detalhes, Integer qtdEstoque, Double preco, Categoria categoria,
			Loja loja) {
		super();
		this.id = id;
		this.nome = nome;
		this.detalhes = detalhes;
		this.qtdEstoque = qtdEstoque;
		this.preco = preco;
		this.categoria = categoria;
		this.loja = loja;
	}
	
	
}
