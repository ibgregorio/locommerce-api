package com.ibgregorio.locommerce.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ibgregorio.locommerce.entity.converter.StatusEnumConverter;
import com.ibgregorio.locommerce.entity.enums.StatusEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Loja implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nomeFantasia;
	
	private String cnpj;
	
	private String descricao;
	
	@Convert(converter = StatusEnumConverter.class)
	private StatusEnum situacao;
	
	private String justificativaReprovacao;
	
	@OneToOne(mappedBy = "loja", cascade = CascadeType.ALL)
	private Endereco endereco;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "id_usuario")
	private Usuario proprietario;
	
	@ManyToOne
	@JoinColumn(name = "id_categoria")
	private Categoria categoria;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "loja", cascade = CascadeType.ALL)
	private List<Produto> produtos = new ArrayList<>();

	public Loja(Long id, String nomeFantasia, String cnpj, String descricao, StatusEnum situacao,
			String justificativaReprovacao, Endereco endereco, Usuario proprietario, Categoria categoria) {
		super();
		this.id = id;
		this.nomeFantasia = nomeFantasia;
		this.cnpj = cnpj;
		this.descricao = descricao;
		this.situacao = situacao;
		this.justificativaReprovacao = justificativaReprovacao;
		this.endereco = endereco;
		this.proprietario = proprietario;
		this.categoria = categoria;
	}

}
