package com.ibgregorio.locommerce.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.dto.ListaDesejoDTO;
import com.ibgregorio.locommerce.entity.ListaDesejo;
import com.ibgregorio.locommerce.entity.Usuario;
import com.ibgregorio.locommerce.repository.ListaDesejoRepository;
import com.ibgregorio.locommerce.security.UserPrincipal;
import com.ibgregorio.locommerce.services.exception.AuthorizationException;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;
import com.ibgregorio.locommerce.services.util.AuthUtils;

@Service
public class ListaDesejoService {

	@Autowired
	private ListaDesejoRepository repository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	public ListaDesejo findListaDesejo(Long id) {
		Optional<ListaDesejo> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Lista de Desejo não encontrada! ID: " + id ));
	}
	
	public List<ListaDesejo> loadListasUsuario() {
		Usuario usuario = verifyAccess();
		
		return repository.findByUsuario(usuario);
	}
	
	public ListaDesejo criarLista(ListaDesejo listaDesejo) {
		Usuario usuario = verifyAccess();
		listaDesejo.setUsuario(usuario);
		
		return repository.save(listaDesejo);
	}
	
	public ListaDesejo editarLista(ListaDesejoDTO listaDesejo) {
		ListaDesejo listUpdate = findListaDesejo(listaDesejo.getId());
		updateFields(listUpdate, listaDesejo);
		
		return repository.save(listUpdate);
	}
	
	public void delete(Long id) {
		ListaDesejo selectedLista = findListaDesejo(id);
		
		repository.deleteById(selectedLista.getId());
	}
	
	
	private void updateFields(ListaDesejo newObj, ListaDesejoDTO listaDto) {
		newObj.setNomeLista(listaDto.getNomeLista());
	}
	
	private Usuario verifyAccess() {
		UserPrincipal user = AuthUtils.authenticated();
		if (user == null) {
			throw new AuthorizationException("Acesso Negado!");
		}
		
		return usuarioService.findUsuario(user.getId());
	}
	
}
