package com.ibgregorio.locommerce.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.entity.Categoria;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.enums.SimNaoEnum;
import com.ibgregorio.locommerce.entity.enums.StatusEnum;
import com.ibgregorio.locommerce.mapper.annotation.DefaultMethodMapping;
import com.ibgregorio.locommerce.repository.CategoriaRepository;
import com.ibgregorio.locommerce.repository.EnderecoRepository;
import com.ibgregorio.locommerce.repository.LojaRepository;
import com.ibgregorio.locommerce.services.email.EmailService;
import com.ibgregorio.locommerce.services.exception.DataIntegrityException;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;

@Service
public class LojaService {
	
	@Autowired
	private LojaRepository repository;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private EmailService emailService;

	@DefaultMethodMapping
	public Loja findLoja(Long id) {
		Optional<Loja> loja = repository.findById(id);
		
		return loja.orElseThrow(() -> new ObjectNotFoundException(
				"Loja não encontrada! ID: " + id));
	}
	
	public Loja findLojaByIdProduto(Long id) {
		Optional<Loja> loja = repository.findLojaByIdProduto(id);
		
		return loja.orElseThrow(() -> new ObjectNotFoundException(
				"Loja não encontrada! ID: " + id));
	}
	
	public List<Loja> searchLojas(String filtroNome, Integer idCategoria, StatusEnum situacao) {
		Optional<Categoria> selectedCategoria = Optional.empty();
		
		if (idCategoria != null) {
			selectedCategoria = categoriaRepository.findById(idCategoria);
		}
		
		
		if (situacao != null) {
			return repository.findBySituacaoAndJustificativaReprovacaoIsNull(situacao);
		} else if (!filtroNome.isEmpty() && selectedCategoria.isPresent()) {
			return repository.findByCategoriaAndNomeFantasiaContainingIgnoreCase(selectedCategoria.orElse(null), filtroNome);
		} else if (selectedCategoria.isPresent()) {
			return repository.findByCategoria(selectedCategoria.orElse(null));
		} else {
			return repository.findByNomeFantasiaContainingIgnoreCase(filtroNome);
		}
	}
	
	public Loja criarLoja(Loja loja) {
		validateLoja(loja);
		
		loja.getEndereco().setLoja(loja);
		loja.getEndereco().setPrincipal(SimNaoEnum.SIM);
		loja.setSituacao(StatusEnum.INATIVO);
		
		loja = repository.save(loja);
		enderecoRepository.save(loja.getEndereco());
		
		return loja;
	}
	
	public Loja update(Loja loja) {
		Loja lojaUpdate = findLoja(loja.getId());
		updateFields(lojaUpdate, loja);
		
		return repository.save(lojaUpdate);
	}
	
	public Loja aprovarLoja(Long idLoja) {
		Loja selectedLoja = findLoja(idLoja);
		validateLiberacaoLoja(selectedLoja, true);
		
		selectedLoja.setSituacao(StatusEnum.ATIVO);
		repository.save(selectedLoja);
		
		emailService.sendLojaAprovadaHtmlEmail(selectedLoja);
		
		return selectedLoja;
	}
	
	public Loja reprovarLoja(Loja loja) {
		Loja selectedLoja = findLoja(loja.getId());
		validateLiberacaoLoja(selectedLoja, false);
		
		selectedLoja.setJustificativaReprovacao(loja.getJustificativaReprovacao());
		repository.save(selectedLoja);
		
		emailService.sendLojaReprovadaHtmlEmail(selectedLoja);
		
		return selectedLoja;
	}
	
	private void updateFields(Loja newObj, Loja obj) {
		newObj.setNomeFantasia(obj.getNomeFantasia());
		
		if (newObj.getCategoria() != null) {
			newObj.setCategoria(obj.getCategoria());			
		}
	}
	
	public void validateLoja(Loja loja) {
		Optional<Loja> lojaExistente = repository.findByCnpj(loja.getCnpj());
		
		if (lojaExistente.isPresent()) {
			throw new DataIntegrityException("Já existe uma loja vinculada ao CNPJ informado");
		}
	}
	
	private void validateLiberacaoLoja(Loja loja, boolean isAprovacao) {
		
		if (isAprovacao && loja.getSituacao().equals(StatusEnum.ATIVO)) {
			throw new DataIntegrityException("A loja selecionada já foi aprovada");
		}
		
		if (!isAprovacao && loja.getSituacao().equals(StatusEnum.ATIVO)) {
			throw new DataIntegrityException("Não é possível reprovar uma loja ativa");
		}
	}
}
