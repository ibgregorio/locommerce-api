package com.ibgregorio.locommerce.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.dto.ItemDesejoDTO;
import com.ibgregorio.locommerce.entity.ItemDesejo;
import com.ibgregorio.locommerce.entity.ListaDesejo;
import com.ibgregorio.locommerce.repository.ItemDesejoRepository;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;

@Service
public class ItemDesejoService {

	@Autowired
	private ItemDesejoRepository repository;
	
	@Autowired
	private ListaDesejoService listaDesejoService;
	
	@Autowired
	private ProdutoService produtoService;
	
	public ItemDesejo findItem(ItemDesejoDTO item) {
		Optional<ItemDesejo> obj = repository.findByListaProduto(item.getIdLista(), item.getIdProduto());
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Item não encontrado!"));
	}
	
	public ItemDesejo adicionarProduto(ItemDesejoDTO item) {
		ItemDesejo novoItem = new ItemDesejo();
		ListaDesejo lista = listaDesejoService.findListaDesejo(item.getIdLista());
		
		novoItem.setListaDesejo(lista);
		novoItem.setProduto(produtoService.findProduto(item.getIdProduto()));
		
		return repository.save(novoItem);		
	}
	
	public void removerProduto(ItemDesejoDTO item) {
		ItemDesejo selectedItem = findItem(item);
		
		repository.delete(selectedItem);
	}
	
}
