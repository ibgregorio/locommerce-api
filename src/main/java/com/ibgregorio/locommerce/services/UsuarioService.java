package com.ibgregorio.locommerce.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.dto.UsuarioNovoDTO;
import com.ibgregorio.locommerce.entity.Categoria;
import com.ibgregorio.locommerce.entity.Cidade;
import com.ibgregorio.locommerce.entity.Endereco;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.Usuario;
import com.ibgregorio.locommerce.entity.enums.AuthProviderEnum;
import com.ibgregorio.locommerce.entity.enums.PerfilUsuarioEnum;
import com.ibgregorio.locommerce.entity.enums.SimNaoEnum;
import com.ibgregorio.locommerce.mapper.annotation.DefaultMethodMapping;
import com.ibgregorio.locommerce.repository.EnderecoRepository;
import com.ibgregorio.locommerce.repository.UsuarioRepository;
import com.ibgregorio.locommerce.security.UserPrincipal;
import com.ibgregorio.locommerce.services.exception.AuthorizationException;
import com.ibgregorio.locommerce.services.exception.DataIntegrityException;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;
import com.ibgregorio.locommerce.services.util.AuthUtils;

@Service
public class UsuarioService  {

	@Autowired
	private UsuarioRepository repository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private CategoriaService categoriaService;
	
	@Autowired
	private LojaService lojaService;
	
	@Autowired
    private PasswordEncoder pe;
		
	@DefaultMethodMapping
	public Usuario findById(Long id) {
		Optional<Usuario> usuario = repository.findById(id);
		
		return usuario.orElseThrow(() -> new ObjectNotFoundException(
				"Usuário não encontrado! ID: " + id));
	}
	
	public Usuario findUsuario(Long id) {
		verifyAccess(id);
		
		return findById(id);
	}
	
	public List<Usuario> findAllUsuarios() {
		return repository.findAll();
	}
	
	public Usuario findByEmail(String email) {
		
		UserPrincipal user = AuthUtils.authenticated();
		if (user == null || !user.hasRole(PerfilUsuarioEnum.ADMIN) && !email.equals(user.getEmail())) {
			throw new AuthorizationException("Acesso Negado!");
		}

		Optional<Usuario> usuario = repository.findByEmailAndDtExclusaoIsNull(email);
		
		return usuario.orElseThrow(() -> new ObjectNotFoundException(
				"Usuário não encontrado! ID: " + user.getEmail()));
	}
	
	@Transactional
	public Usuario cadastrar(Usuario usuario) {
		usuario = repository.save(usuario);
		
		if (usuario.getNovaLoja() != null) {
			lojaService.criarLoja(usuario.getNovaLoja());
		}
		
		if (usuario.getEnderecos() != null && !usuario.getEnderecos().isEmpty()) {
			enderecoRepository.saveAll(usuario.getEnderecos());
		}
		
		return usuario;
	}
	
	public Usuario update(Usuario usuario) {
		Usuario usrUpdate = findUsuario(usuario.getId());
		updateFields(usrUpdate, usuario);
		
		return repository.save(usrUpdate);
	}
	
	public void delete(Long id) {
		Usuario selectedUsuario = findUsuario(id);
		
		try {
			selectedUsuario.setDtExclusao(new Date());
			repository.save(selectedUsuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Usuario generateNewUsuario(UsuarioNovoDTO usuarioNovo) {
		
		validateNovoUsuario(usuarioNovo);
		
		Usuario entity = new Usuario(null, usuarioNovo.getNome(), usuarioNovo.getEmail(), pe.encode(usuarioNovo.getSenha()), 
				usuarioNovo.getCpf(), usuarioNovo.getNumeTel(), usuarioNovo.getNumeCelular(), null, AuthProviderEnum.DEFAULT, null, null);
		entity.addPerfil(PerfilUsuarioEnum.toEnum(usuarioNovo.getTipoUsuario()));
		
		if (usuarioNovo.getTipoUsuario().equals(2)) {
			Cidade cidade = new Cidade(usuarioNovo.getLoja().getEndereco().getIdCidade(), null, null);
			Endereco endLoja = new Endereco(null, usuarioNovo.getLoja().getEndereco().getLogradouro(), usuarioNovo.getLoja().getEndereco().getNumero(), 
					usuarioNovo.getLoja().getEndereco().getComplemento(), 
					usuarioNovo.getLoja().getEndereco().getBairro(), usuarioNovo.getLoja().getEndereco().getCep(), cidade, SimNaoEnum.SIM, null, null);
			Categoria catLoja = categoriaService.findCategoria(usuarioNovo.getLoja().getIdCategoria());
			
			Loja lojaVendedor = new Loja(null, usuarioNovo.getLoja().getNomeFantasia(), usuarioNovo.getLoja().getCnpj(), null, null, null, endLoja, entity, catLoja);
			lojaService.validateLoja(lojaVendedor);
			entity.setNovaLoja(lojaVendedor);
		} else {
			Cidade cidade = new Cidade(usuarioNovo.getEndereco().getIdCidade(), null, null);
			Endereco endUsuario = new Endereco(null, usuarioNovo.getEndereco().getLogradouro(), usuarioNovo.getEndereco().getNumero(), usuarioNovo.getEndereco().getComplemento(), 
					usuarioNovo.getEndereco().getBairro(), usuarioNovo.getEndereco().getCep(), cidade, SimNaoEnum.SIM, entity, null);
			
			entity.getEnderecos().add(endUsuario);			
		}
		
		return entity;
	}
	
	private void updateFields(Usuario newObj, Usuario obj) {
		newObj.setNome(obj.getNome());
		newObj.setEmail(obj.getEmail());
		newObj.setNumeTel(obj.getNumeTel());
		newObj.setNumeCelular(obj.getNumeCelular());
	}
	
	private void verifyAccess(Long id) {
		UserPrincipal user = AuthUtils.authenticated();
		if (user == null || !user.hasRole(PerfilUsuarioEnum.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso Negado!");
		}
	}
	
	private void validateNovoUsuario(UsuarioNovoDTO novoUsuario) {
		if (repository.existsByEmail(novoUsuario.getEmail())) {
            throw new DataIntegrityException("O e-mail informado já está cadastrado");
        }
		
		if (novoUsuario.getTipoUsuario().equals(2) && novoUsuario.getLoja() == null) {
			throw new DataIntegrityException("Informe os dados da loja");
		}
		
		if (novoUsuario.getTipoUsuario().equals(3) && novoUsuario.getEndereco() == null) {
			throw new DataIntegrityException("Informe o seu endereço");
		}
	}
	
}
