package com.ibgregorio.locommerce.services.util;

import org.springframework.security.core.context.SecurityContextHolder;

import com.ibgregorio.locommerce.security.UserPrincipal;

public class AuthUtils {

	public static UserPrincipal authenticated() {
		try {
			return (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
			return null;
		}
	}
}
