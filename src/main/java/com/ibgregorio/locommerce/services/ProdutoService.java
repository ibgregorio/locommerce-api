package com.ibgregorio.locommerce.services;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.entity.Categoria;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.Produto;
import com.ibgregorio.locommerce.repository.CategoriaRepository;
import com.ibgregorio.locommerce.repository.ProdutoRepository;
import com.ibgregorio.locommerce.services.exception.DataIntegrityException;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository repository;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private LojaService lojaService;
	
	public Produto findProduto(Long id) {
		Optional<Produto> produto = repository.findById(id);
		
		return produto.orElseThrow(() -> new ObjectNotFoundException(
				"Produto não encontrado! ID: " + id));
	}
	
	public List<Produto> searchProdutos(String filtroNome, Integer idCategoria) {
		Optional<Categoria> selectedCategoria = Optional.empty();
		
		if (idCategoria != null) {
			selectedCategoria = categoriaRepository.findById(idCategoria);
		}
		
		if (!filtroNome.isEmpty() && selectedCategoria.isPresent()) {
			return repository.findByCategoriaAndNomeContainingIgnoreCase(selectedCategoria.orElse(null), filtroNome);
		} else if (selectedCategoria.isPresent()) {
			return repository.findByCategoria(selectedCategoria.orElse(null));
		} else {
			return repository.findByNomeContainingIgnoreCase(filtroNome);
		}
		
	}
	
	public List<Produto> loadProdutosFromLoja(Long idLoja) {
		Loja selectedLoja = lojaService.findLoja(idLoja);
		
		return repository.findByLoja(selectedLoja);
	}
	
	public Produto criarProduto(Produto produto) {
		produto.setDtAtualizacaoPreco(new Date());
		
		return repository.save(produto);
	}
	
	public Produto update(Produto produto) {
		Produto catUpdate = findProduto(produto.getId());
		validateProduto(catUpdate, produto);
		updateFields(catUpdate, produto);
		catUpdate.setDtAtualizacaoPreco(new Date());
		
		return repository.save(catUpdate);
	}
	
	public void deleteProduto(Long id) {
		Produto selectedProduto = findProduto(id);
		validateProdutoDelete(selectedProduto);
		
		repository.deleteById(id);
	}
	
	private void updateFields(Produto newObj, Produto obj) {
		newObj.setNome(obj.getNome());
		newObj.setDetalhes(obj.getDetalhes());
		newObj.setPreco(obj.getPreco());
		newObj.setQtdEstoque(obj.getQtdEstoque());
		
		if (newObj.getCategoria() != null) {
			newObj.setCategoria(obj.getCategoria());
		}
	}
	
	private void validateProdutoDelete(Produto produto) {
		if (produto.getQtdEstoque() > 0) {
			throw new DataIntegrityException("Não é possível remover um produto com estoque");
		}
	}
	
	private void validateProduto(Produto selectedProduto, Produto produtoAtualizado) {
		LocalDate dataProxAtualizacao = selectedProduto.getDtAtualizacaoPreco().toInstant()
				.atZone(ZoneId.systemDefault()).toLocalDate().plusWeeks(1);
		
		if (!selectedProduto.getPreco().equals(produtoAtualizado.getPreco()) && dataProxAtualizacao.isAfter(LocalDate.now())) {
			throw new DataIntegrityException("O preço somente pode ser alterado 1 semana depois da última atualização");
		}
	}
}
