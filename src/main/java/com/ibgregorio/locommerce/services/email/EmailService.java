package com.ibgregorio.locommerce.services.email;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;

import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.Pedido;

public interface EmailService {

	void sendConfirmacaoPedidoEmail(Pedido obj);
	
	void sendLojaAprovadaEmail(Loja obj);
	
	void sendLojaReprovadaEmail(Loja obj);

	void sendEmail(SimpleMailMessage msg);
	
	void sendConfirmacaoPedidoHtmlEmail(Pedido obj);
	
	void sendLojaAprovadaHtmlEmail(Loja obj);
	
	void sendLojaReprovadaHtmlEmail(Loja obj);

	void  sendHtmlEmail(MimeMessage msg);
}
