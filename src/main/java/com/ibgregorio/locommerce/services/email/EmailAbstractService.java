package com.ibgregorio.locommerce.services.email;

import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.Pedido;

public abstract class EmailAbstractService implements EmailService {

	@Value("${default.sender}")
	private String sender;
	
	@Autowired
	private TemplateEngine templateEngine;

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Override
	public void sendConfirmacaoPedidoEmail(Pedido obj) {
		SimpleMailMessage sm = prepareMailMessagePedidoConfirmado(obj);
		sendEmail(sm);
	}
	
	@Override
	public void sendLojaAprovadaEmail(Loja obj) {
		SimpleMailMessage sm = prepareMailLojaAprovada(obj);
		sendEmail(sm);
	}
	
	@Override
	public void sendLojaReprovadaEmail(Loja obj) {
		SimpleMailMessage sm = prepareMailLojaReprovada(obj);
		sendEmail(sm);
	}

	protected SimpleMailMessage prepareMailLojaAprovada(Loja obj) {
		SimpleMailMessage sm = new SimpleMailMessage();
		sm.setTo(obj.getProprietario().getEmail());
		sm.setFrom(sender);
		sm.setSubject("Sua loja foi aprovada com sucesso!");
		sm.setSentDate(new Date(System.currentTimeMillis()));
		sm.setText(obj.toString());

		return sm;
	}
	
	protected SimpleMailMessage prepareMailLojaReprovada(Loja obj) {
		SimpleMailMessage sm = new SimpleMailMessage();
		sm.setTo(obj.getProprietario().getEmail());
		sm.setFrom(sender);
		sm.setSubject("Sua loja foi reprovada");
		sm.setSentDate(new Date(System.currentTimeMillis()));
		sm.setText(obj.toString());

		return sm;
	}
	
	protected SimpleMailMessage prepareMailMessagePedidoConfirmado(Pedido obj) {
		SimpleMailMessage sm = new SimpleMailMessage();
		sm.setTo(obj.getComprador().getEmail());
		sm.setFrom(sender);
		sm.setSubject("Pedido nº " + obj.getId() + " confirmado!");
		sm.setSentDate(new Date(System.currentTimeMillis()));
		sm.setText(obj.toString());

		return sm;
	}

	protected String htmlFromTemplatePedido(Pedido obj) {
		Context context = new Context();
		context.setVariable("pedido", obj);
		return templateEngine.process("email/confirmacaoPedidoTemplate", context);
	}

	@Override
	public void sendConfirmacaoPedidoHtmlEmail(Pedido obj) {

		try {
			MimeMessage mm = prepareMimeMessageFromPedido(obj);
			sendHtmlEmail(mm);
		} catch (MessagingException e) {
			sendConfirmacaoPedidoEmail(obj);
		}
	}
	
	@Override
	public void sendLojaAprovadaHtmlEmail(Loja obj) {

		try {
			MimeMessage mm = prepareMimeMessageLojaAprovada(obj);
			sendHtmlEmail(mm);
		} catch (MessagingException e) {
			sendLojaAprovadaEmail(obj);
		}
	}
	
	@Override
	public void sendLojaReprovadaHtmlEmail(Loja obj) {

		try {
			MimeMessage mm = prepareMimeMessageLojaReprovada(obj);
			sendHtmlEmail(mm);
		} catch (MessagingException e) {
			sendLojaReprovadaEmail(obj);
		}
	}

	protected MimeMessage prepareMimeMessageFromPedido(Pedido obj) throws MessagingException {
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper mmh = new MimeMessageHelper(mimeMessage, true);
		mmh.setTo(obj.getComprador().getEmail());
		mmh.setFrom(sender);
		mmh.setSubject("Pedido nº " + obj.getId() + " confirmado!");
		mmh.setSentDate(new Date(System.currentTimeMillis()));
		mmh.setText(htmlFromTemplatePedido(obj), true);

		return mimeMessage;
	}
	
	protected MimeMessage prepareMimeMessageLojaAprovada(Loja obj) throws MessagingException {
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper mmh = new MimeMessageHelper(mimeMessage, true);
		mmh.setTo(obj.getProprietario().getEmail());
		mmh.setFrom(sender);
		mmh.setSubject("Sua loja foi aprovada com sucesso!");
		mmh.setSentDate(new Date(System.currentTimeMillis()));
		mmh.setText(htmlFromTemplateAprovacao(obj), true);

		return mimeMessage;
	}
	
	protected MimeMessage prepareMimeMessageLojaReprovada(Loja obj) throws MessagingException {
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper mmh = new MimeMessageHelper(mimeMessage, true);
		mmh.setTo(obj.getProprietario().getEmail());
		mmh.setFrom(sender);
		mmh.setSubject("Sua loja foi reprovada");
		mmh.setSentDate(new Date(System.currentTimeMillis()));
		mmh.setText(htmlFromTemplateReprovacao(obj), true);

		return mimeMessage;
	}
	
	protected String htmlFromTemplateAprovacao(Loja obj) {
		Context context = new Context();
		context.setVariable("loja", obj);
		return templateEngine.process("email/lojaAprovadaTemplate", context);
	}
	
	protected String htmlFromTemplateReprovacao(Loja obj) {
		Context context = new Context();
		context.setVariable("loja", obj);
		return templateEngine.process("email/lojaReprovadaTemplate", context);
	}
}
