package com.ibgregorio.locommerce.services.email;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;

public class EmailMockService extends EmailAbstractService {

	private static final Logger LOG = LoggerFactory.getLogger(EmailMockService.class);

	@Override
	public void sendEmail(SimpleMailMessage msg) {
		LOG.info("Testando envio de e-mail...");
		LOG.info(msg.toString());
		LOG.info("E-mail enviado com sucesso!");
	}
	
	@Override
	public void sendHtmlEmail(MimeMessage msg) {
		LOG.info("Simulando envio de email em HTML...");
		LOG.info(msg.toString());
		LOG.info("Email enviado");
	}
	
}
