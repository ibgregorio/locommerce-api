package com.ibgregorio.locommerce.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.entity.Avaliacao;
import com.ibgregorio.locommerce.repository.AvaliacaoRepository;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;

@Service
public class AvaliacaoService {

	@Autowired
	private AvaliacaoRepository repository;
	
	public Avaliacao findAvaliacao(Long id) {
		Optional<Avaliacao> avaliacao = repository.findById(id);
		
		return avaliacao.orElseThrow(() -> new ObjectNotFoundException(
				"Avaliacao selecionada não foi encontrada! ID: " + id));
	}
	
	public List<Avaliacao> findAllAvaliacoes() {
		return repository.findAll();
	}
	
	public Avaliacao save(Avaliacao avaliacao) {
		avaliacao.setId(null);
		avaliacao.setDtAvaliacao(new Date());
		return repository.save(avaliacao);
	}
	
	public Avaliacao editarAvaliacao(Avaliacao avaliacao) {
		Avaliacao avUpdate = findAvaliacao(avaliacao.getId());
		avaliacao.setDtAvaliacao(new Date());
		updateFields(avUpdate, avaliacao);
		
		return repository.save(avUpdate);
	}
	
	public void removerAvaliacao(Long id) {
		findAvaliacao(id);
		
		repository.deleteById(id);
	}
	
	private void updateFields(Avaliacao avUpdate, Avaliacao avaliacao) {
		avUpdate.setNota(avaliacao.getNota());
	}
	
}
