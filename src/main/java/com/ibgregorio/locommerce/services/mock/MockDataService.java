package com.ibgregorio.locommerce.services.mock;

import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.entity.Categoria;
import com.ibgregorio.locommerce.entity.Cidade;
import com.ibgregorio.locommerce.entity.Endereco;
import com.ibgregorio.locommerce.entity.Estado;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.Produto;
import com.ibgregorio.locommerce.entity.Usuario;
import com.ibgregorio.locommerce.entity.enums.AuthProviderEnum;
import com.ibgregorio.locommerce.entity.enums.PerfilUsuarioEnum;
import com.ibgregorio.locommerce.entity.enums.SimNaoEnum;
import com.ibgregorio.locommerce.entity.enums.StatusEnum;
import com.ibgregorio.locommerce.repository.CategoriaRepository;
import com.ibgregorio.locommerce.repository.CidadeRepository;
import com.ibgregorio.locommerce.repository.EnderecoRepository;
import com.ibgregorio.locommerce.repository.EstadoRepository;
import com.ibgregorio.locommerce.repository.ProdutoRepository;
import com.ibgregorio.locommerce.repository.UsuarioRepository;
import com.ibgregorio.locommerce.services.LojaService;

@Service
public class MockDataService {
	
	@Autowired
	private EstadoRepository estadoRepository;

	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
    private PasswordEncoder pe;
	
	@Autowired
	private LojaService lojaService;

	public void initDatabase() throws Exception {
		Categoria catTeste = new Categoria(null, "Vestuario");
		Categoria catTest2 = new Categoria(null, "Calçados");
		Categoria catTest3 = new Categoria(null, "Alimentos");
		Categoria catTest4 = new Categoria(null, "Artesanato");
		Categoria catTest5 = new Categoria(null, "Para Casa");
		categoriaRepository.saveAll(Arrays.asList(catTeste, catTest2, catTest3, catTest4, catTest5));
		
		Estado est1 = new Estado(null, "Minas Gerais");
		Estado est2 = new Estado(null, "São Paulo");

		Cidade c1 = new Cidade(null, "Belo Horizonte", est1);
		Cidade c2 = new Cidade(null, "São Paulo", est2);
		Cidade c3 = new Cidade(null, "Santos", est2);

		est1.getCidades().addAll(Arrays.asList(c1));
		est2.getCidades().addAll(Arrays.asList(c2, c3));

		estadoRepository.saveAll(Arrays.asList(est1, est2));
		cidadeRepository.saveAll(Arrays.asList(c1, c2, c3));
		
		Usuario usuarioTeste = new Usuario(null,  "Ítalo Gregório", "italo@teste.com", pe.encode("123456"), "83212078049", "(71) 99999999", null, null, AuthProviderEnum.DEFAULT, null, null);
		usuarioTeste.addPerfil(PerfilUsuarioEnum.VENDEDOR);
		
		Usuario usuarioTeste2 = new Usuario(null,  "Ítalo Gregório Comprador", "italocompras@teste.com", pe.encode("abcdef"), "70536320080", "(71) 888888888", null, null, AuthProviderEnum.DEFAULT, null, null);
		usuarioTeste2.addPerfil(PerfilUsuarioEnum.CLIENTE);
		
		Usuario usuarioAdmin = new Usuario(null,  "Administrador", "admin@teste.com", pe.encode("admin123"), "93991353067", "(71) 888888888", null, null, AuthProviderEnum.DEFAULT, null, null);
		usuarioAdmin.addPerfil(PerfilUsuarioEnum.ADMIN);
		
		usuarioRepository.saveAll(Arrays.asList(usuarioTeste, usuarioTeste2, usuarioAdmin));
		
		Endereco enderecoLoja = new Endereco(null, "Av. Paulista", "123", "Bloco 20", "Centro", "35680-622", c2, null, null, null);
		Endereco enderecoCliente = new Endereco(null, "Rua 13 de Março", "456", null, "Liberdade", "97230-970", c3, SimNaoEnum.SIM, usuarioTeste2, null);
		
		enderecoRepository.save(enderecoCliente);
		
		Loja loja = new Loja(null, "Sapatos Baltazar", "48.956.881/0001-45", "Descrição da loja para teste", StatusEnum.ATIVO, null, enderecoLoja, usuarioTeste, catTest2);
		lojaService.criarLoja(loja);
		
		Produto produto = new Produto(null, "Sapatenis Cinza", "Lorem ipsum auctor molestie taciti sodales aliquet purus suscipit, ante morbi tincidunt curabitur litora nam eget viverra nulla, cras nunc placerat scelerisque nullam eget est.", 
				5, 79.9, catTest2, loja);
		produto.setDtAtualizacaoPreco(new Date());
		Produto produto2 = new Produto(null, "Sandália Branca", "Lorem ipsum auctor dois molestie taciti sodales aliquet purus suscipit, ante morbi tincidunt curabitur litora nam eget viverra nulla, cras nunc placerat scelerisque nullam eget est.", 
				45, 19.9, catTest2, loja);
		produto2.setDtAtualizacaoPreco(new Date());
		Produto produto3 = new Produto(null, "Sapatenis Azul 3", "Lorem ipsum auctor molestie taciti sodales aliquet purus suscipit, ante morbi tincidunt curabitur litora nam eget viverra nulla, cras nunc placerat scelerisque nullam eget est.", 
				0, 39.9, catTeste, loja);
		produto3.setDtAtualizacaoPreco(new Date());
		Produto produto4 = new Produto(null, "Sapatenis Cinza 4", "Lorem ipsum auctor molestie taciti sodales aliquet purus suscipit, ante morbi tincidunt curabitur litora nam eget viverra nulla, cras nunc placerat scelerisque nullam eget est.", 
				10, 79.9, catTest2, loja);
		produto4.setDtAtualizacaoPreco(new Date());
		Produto produto5 = new Produto(null, "Sapatenis Bege 5", "Lorem ipsum auctor molestie taciti sodales aliquet purus suscipit, ante morbi tincidunt curabitur litora nam eget viverra nulla, cras nunc placerat scelerisque nullam eget est.", 
				245, 109.9, catTest2, loja);
		produto5.setDtAtualizacaoPreco(new Date());
		Produto produto6 = new Produto(null, "Sapatenis Cinza 6", "Lorem ipsum auctor molestie taciti sodales aliquet purus suscipit, ante morbi tincidunt curabitur litora nam eget viverra nulla, cras nunc placerat scelerisque nullam eget est.", 
				7, 29.9, catTest2, loja);
		produto6.setDtAtualizacaoPreco(new Date());
		Produto produto7 = new Produto(null, "Sapatenis Branco 7", "Lorem ipsum auctor molestie taciti sodales aliquet purus suscipit, ante morbi tincidunt curabitur litora nam eget viverra nulla, cras nunc placerat scelerisque nullam eget est.", 
				30, 79.9, catTeste, loja);
		produto7.setDtAtualizacaoPreco(new Date());
		
		produtoRepository.saveAll(Arrays.asList(produto, produto, produto2, produto3, produto4, produto5, produto6, produto7));
	}
}
