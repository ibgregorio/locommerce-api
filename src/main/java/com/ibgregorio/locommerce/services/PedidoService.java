package com.ibgregorio.locommerce.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.dto.PedidoDTO;
import com.ibgregorio.locommerce.entity.Pedido;
import com.ibgregorio.locommerce.entity.PedidoItem;
import com.ibgregorio.locommerce.entity.Usuario;
import com.ibgregorio.locommerce.entity.enums.PerfilUsuarioEnum;
import com.ibgregorio.locommerce.entity.enums.StatusPedidoEnum;
import com.ibgregorio.locommerce.repository.PedidoItemRepository;
import com.ibgregorio.locommerce.repository.PedidoRepository;
import com.ibgregorio.locommerce.security.UserPrincipal;
import com.ibgregorio.locommerce.services.email.EmailService;
import com.ibgregorio.locommerce.services.exception.AuthorizationException;
import com.ibgregorio.locommerce.services.exception.DataIntegrityException;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;
import com.ibgregorio.locommerce.services.util.AuthUtils;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository repository;
	
	@Autowired
	private PedidoItemRepository itemRepository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private EmailService emailService;
	
	public Pedido findPedido(Long id) {
		Optional<Pedido> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Pedido não encontrado! ID: " + id ));
	}
	
	public List<Pedido> loadPedidosComprador() {
		Usuario comprador = verifyAccessCliente();
		
		return repository.findByComprador(comprador);
	}
	
	public List<Pedido> loadPedidosVendedor() {
		Usuario comprador = verifyAccessVendedor();
		
		return repository.findByLoja(comprador.getLoja().getId());
	}
	
	@Transactional
	public Pedido finalizarPedido(Pedido pedido) {
		pedido.setDataPedido(new Date());
		pedido.setDataAtualizacao(new Date());
		pedido.setStatus(StatusPedidoEnum.PENDENTE);
		pedido.setComprador(usuarioService.findUsuario(pedido.getComprador().getId()));
		
		pedido = repository.save(pedido);
		
		for (PedidoItem item : pedido.getItensPedido()) {
			item.setDesconto(0.0); //FIXME encontrar uma melhor forma de aplicar o desconto aos itens do pedido
			item.setProduto(produtoService.findProduto(item.getProduto().getId()));
			item.setPreco(item.getProduto().getPreco());
			item.setPedido(pedido);
		}
		
		itemRepository.saveAll(pedido.getItensPedido());
		
		emailService.sendConfirmacaoPedidoHtmlEmail(pedido);
		
		return pedido;
		
	}
	
	public Pedido atualizarStatus(PedidoDTO pedidoDto) {
		Pedido pediUpdate = findPedido(pedidoDto.getId());
		validateStatus(pediUpdate, pedidoDto);
		updateFields(pediUpdate, pedidoDto);
		
		return repository.save(pediUpdate);
	}
	
	public void cancelarPedido(Long id) {
		Pedido pediCancelado = findPedido(id);
		validateCancelamento(pediCancelado);
		
		try {
			pediCancelado.setStatus(StatusPedidoEnum.CANCELADO);
			pediCancelado.setDataAtualizacao(new Date());
			
			repository.save(pediCancelado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void updateFields(Pedido newObj, PedidoDTO pedidoDto) {
		newObj.setStatus(StatusPedidoEnum.toEnum(pedidoDto.getStatus()));
		newObj.setDataAtualizacao(new Date());
	}
	
	private void validateStatus(Pedido pedido, PedidoDTO pedidoAtualizado) {
		if (pedido.getStatus().equals(StatusPedidoEnum.ENTREGUE)) {
			throw new DataIntegrityException("O pedido selecionado já foi entregue!");
		}
		
		if (pedido.getStatus().equals(StatusPedidoEnum.EMTRANSITO) 
				&& (!"T".equals(pedidoAtualizado.getStatus()) && !"E".equals(pedidoAtualizado.getStatus()))) {
			throw new DataIntegrityException("Não foi possível atualizar o status para a opção selecionada: o pedido está em trânsito");
		}
		
	}
	
	private void validateCancelamento(Pedido pedido) {
		if (!pedido.getStatus().equals(StatusPedidoEnum.PENDENTE)) {
			throw new DataIntegrityException("Não é possível cancelar: o pedido foi aprovado");
		}
	}
	
	private Usuario verifyAccessCliente() {
		UserPrincipal user = AuthUtils.authenticated();
		if (user == null) {
			throw new AuthorizationException("Acesso Negado!");
		}
		
		return usuarioService.findUsuario(user.getId());
	}
	
	private Usuario verifyAccessVendedor() {
		UserPrincipal user = AuthUtils.authenticated();
		if (user == null || !user.hasRole(PerfilUsuarioEnum.VENDEDOR)) {
			throw new AuthorizationException("Acesso Negado!");
		}
		
		return usuarioService.findUsuario(user.getId());
	}
}
