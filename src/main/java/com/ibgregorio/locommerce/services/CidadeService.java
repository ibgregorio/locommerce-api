package com.ibgregorio.locommerce.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.entity.Cidade;
import com.ibgregorio.locommerce.repository.CidadeRepository;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository repository;
	
	public Cidade findCidade(Integer id) {
		Optional<Cidade> cidade = repository.findById(id);
		
		return cidade.orElseThrow(() -> new ObjectNotFoundException(
				"Cidade não encontrada! ID: " + id));
	}
	
	public List<Cidade> findCidadesByEstado(Integer idEstado) {
		return repository.findCidadesByEstado(idEstado);
	}
}
