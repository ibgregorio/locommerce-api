package com.ibgregorio.locommerce.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.entity.Endereco;
import com.ibgregorio.locommerce.entity.Usuario;
import com.ibgregorio.locommerce.entity.enums.SimNaoEnum;
import com.ibgregorio.locommerce.repository.EnderecoRepository;
import com.ibgregorio.locommerce.security.UserPrincipal;
import com.ibgregorio.locommerce.services.exception.DataIntegrityException;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;
import com.ibgregorio.locommerce.services.util.AuthUtils;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository repository;
	
	@Autowired
	private UsuarioService usuarioService;
		
	public Endereco findEndereco(Long id) {
		Optional<Endereco> endereco = repository.findById(id);
		
		return endereco.orElseThrow(() -> new ObjectNotFoundException(
				"Endereco não encontrado! ID: " + id));
	}
	
	public List<Endereco> findEnderecoByUsuario() {
		UserPrincipal userLogado = AuthUtils.authenticated();
		Usuario usuario = usuarioService.findUsuario(userLogado.getId());
		
		return repository.findByUsuario(usuario);
	}
	
	public Endereco adicionarEndereco(Endereco endereco) {
		endereco.setId(null);
		validateEndereco(endereco);
		
		return repository.save(endereco);
	}
	
	public Endereco update(Endereco endereco) {
		validateEndereco(endereco);
		Endereco endUpdate = findEndereco(endereco.getId());
		updateFields(endUpdate, endereco);
		
		return repository.save(endUpdate);
	}
	
	public void delete(Long id) {
		Endereco selectedEndereco = findEndereco(id);
		checkEnderecoDelete(selectedEndereco);
		
		repository.deleteById(id);
	}

	private void updateFields(Endereco newObj, Endereco obj) {
		newObj.setLogradouro(obj.getLogradouro());
		newObj.setNumero(obj.getNumero());
		newObj.setComplemento(obj.getComplemento());
		newObj.setBairro(obj.getBairro());
		newObj.setCep(obj.getCep());
		newObj.setCidade(obj.getCidade());
		newObj.setPrincipal(obj.getPrincipal());
	}
	
	private void checkEnderecoDelete(Endereco selectedEndereco) {
		if (selectedEndereco.getPrincipal().equals(SimNaoEnum.SIM))
			throw new DataIntegrityException("O endereço principal não pode ser excluído!");
	}
	
	private void validateEndereco(Endereco endereco) {
		Optional<Endereco> enderecoPrincipal = repository.findByUsuarioAndPrincipal(endereco.getUsuario(), SimNaoEnum.SIM);
		
		if (enderecoPrincipal.isPresent()) {
			if ((endereco.getId() == null || !enderecoPrincipal.get().getId().equals(endereco.getId())) 
					&& endereco.getPrincipal().equals(SimNaoEnum.SIM)) {
				throw new DataIntegrityException("Já existe um endereço principal cadastrado");
			}
			
			if (!enderecoPrincipal.get().getCidade().getId().equals(endereco.getCidade().getId())) {
				throw new DataIntegrityException("O novo endereço deve estar situado na mesma cidade do principal!");
			}
			
		}
		
		
	}
	
}
