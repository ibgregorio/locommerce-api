package com.ibgregorio.locommerce.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibgregorio.locommerce.entity.Categoria;
import com.ibgregorio.locommerce.repository.CategoriaRepository;
import com.ibgregorio.locommerce.services.exception.DataIntegrityException;
import com.ibgregorio.locommerce.services.exception.ObjectNotFoundException;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository repository;
	
	public Categoria findCategoria(Integer id) {
		Optional<Categoria> categoria = repository.findById(id);
		
		return categoria.orElseThrow(() -> new ObjectNotFoundException(
				"Categoria selecionada não foi encontrada! ID: " + id));
	}
	
	public List<Categoria> findAllCategorias() {
		return repository.findAll();
	}
	
	public Categoria save(Categoria categoria) {
		categoria.setId(null);
		return repository.save(categoria);
	}
	
	public Categoria update(Categoria categoria) {
		Categoria catUpdate = findCategoria(categoria.getId());
		updateFields(catUpdate, categoria);
		
		return repository.save(catUpdate);
	}
	
	public void delete(Integer id) {
		Categoria selectedCategoria = findCategoria(id);
		checkCategoria(selectedCategoria);
		
		repository.deleteById(id);
	}
	
	private void updateFields(Categoria catUpdate, Categoria categoria) {
		catUpdate.setDescricao(categoria.getDescricao());
	}
	
	private void checkCategoria(Categoria selectedCategoria) {
		if (selectedCategoria.getLojas() != null && !selectedCategoria.getLojas().isEmpty())
			throw new DataIntegrityException("Não foi possível excluir: categoria selecionada possui lojas vinculadas!");
	}
}
