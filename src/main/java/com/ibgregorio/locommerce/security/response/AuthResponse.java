package com.ibgregorio.locommerce.security.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthResponse {
	
	private String email;
	
	private String perfil;

	public AuthResponse(String email, String perfil) {
		this.email = email;
		this.perfil = perfil;
	}

}
