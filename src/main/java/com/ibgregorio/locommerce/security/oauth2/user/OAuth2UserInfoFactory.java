package com.ibgregorio.locommerce.security.oauth2.user;

import java.util.Map;

import com.ibgregorio.locommerce.entity.enums.AuthProviderEnum;
import com.ibgregorio.locommerce.security.exception.OAuth2AuthenticationProcessingException;

public class OAuth2UserInfoFactory {

	public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        if(registrationId.equalsIgnoreCase(AuthProviderEnum.GOOGLE.toString())) {
            return new GoogleOAuth2UserInfo(attributes);
        } else {
            throw new OAuth2AuthenticationProcessingException("O login com " + registrationId + " ainda não está ativado.");
        }
    }
}
