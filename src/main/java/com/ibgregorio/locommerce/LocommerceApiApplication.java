package com.ibgregorio.locommerce;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.ibgregorio.locommerce.config.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class LocommerceApiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(LocommerceApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}
