package com.ibgregorio.locommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.entity.Pedido;
import com.ibgregorio.locommerce.entity.Usuario;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {

	@Transactional(readOnly = true)
	List<Pedido> findByComprador(Usuario comprador);
	
	@Transactional(readOnly = true)
	@Query("select p from Pedido p join p.itensPedido i where i.id.produto.loja.id = ?1")
	List<Pedido> findByLoja(Long id);
}
