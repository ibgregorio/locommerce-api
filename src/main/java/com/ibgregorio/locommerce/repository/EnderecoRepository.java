package com.ibgregorio.locommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.entity.Endereco;
import com.ibgregorio.locommerce.entity.Usuario;
import com.ibgregorio.locommerce.entity.enums.SimNaoEnum;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

	@Transactional(readOnly = true)
	List<Endereco> findByUsuario(Usuario usuario);
	
	Optional<Endereco> findByUsuarioAndPrincipal(Usuario usuario, SimNaoEnum principal);
}
