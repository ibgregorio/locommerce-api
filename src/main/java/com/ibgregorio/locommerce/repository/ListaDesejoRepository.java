package com.ibgregorio.locommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.entity.ListaDesejo;
import com.ibgregorio.locommerce.entity.Usuario;

@Repository
public interface ListaDesejoRepository extends JpaRepository<ListaDesejo, Long> {

	@Transactional(readOnly = true)
	List<ListaDesejo> findByUsuario(Usuario comprador);
	
}
