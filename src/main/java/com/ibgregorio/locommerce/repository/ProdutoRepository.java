package com.ibgregorio.locommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.entity.Categoria;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	@Transactional(readOnly = true)
	List<Produto> findByCategoria(Categoria filtroCategoria);

	@Transactional(readOnly = true)
	List<Produto> findByNomeContainingIgnoreCase(String filtroNome);
	
	@Transactional(readOnly = true)
	List<Produto> findByCategoriaAndNomeContainingIgnoreCase(Categoria filtroCategoria, String filtroNome);
	
	@Transactional(readOnly = true)
	List<Produto> findByLoja(Loja loja);
}
