package com.ibgregorio.locommerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.entity.ItemDesejo;

@Repository
public interface ItemDesejoRepository extends JpaRepository<ItemDesejo, Long> {

	@Transactional(readOnly = true)
	@Query("select i from ItemDesejo i where i.id.listaDesejo.id = ?1 and i.id.produto.id = ?2")
	Optional<ItemDesejo> findByListaProduto(Long idLista, Long idProduto);
}
