package com.ibgregorio.locommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.locommerce.entity.Categoria;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.enums.StatusEnum;

@Repository
public interface LojaRepository extends JpaRepository<Loja, Long> {
	
	@Transactional(readOnly = true)
	List<Loja> findByCategoria(Categoria filtroCategoria);

	@Transactional(readOnly = true)
	List<Loja> findByNomeFantasiaContainingIgnoreCase(String filtroNome);
	
	@Transactional(readOnly = true)
	List<Loja> findByCategoriaAndNomeFantasiaContainingIgnoreCase(Categoria filtroCategoria, String filtroNome);
	
	@Transactional(readOnly = true)
	List<Loja> findBySituacaoAndJustificativaReprovacaoIsNull(StatusEnum situacao);
	
	@Transactional(readOnly = true)
	Optional<Loja> findByCnpj(String cnpj);
	
	@Transactional(readOnly = true)
	@Query("select l from Loja l join l.produtos p where p.id = ?1")
	Optional<Loja> findLojaByIdProduto(Long id);
	
}
