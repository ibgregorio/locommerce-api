package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProdutoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotEmpty(message = "Campo obrigatório")
	@Length(max = 100, message = "O nome do produto deve ter no máx. 100 caracteres")
	private String nome;
	
	@NotEmpty(message = "Campo obrigatório")
	@Length(max = 2000, message = "A descrição do produto deve ter no máx. 2000 caracteres")
	private String detalhes;
	
	@NotNull(message = "Campo obrigatório")
	@Min(value = 1, message = "Informe a quantidade de produtos em estoque")
	private Integer qtdEstoque;
	
	@NotNull(message = "Campo obrigatório")
	@Range(min = 1, message = "O preço deve ser maior do que 1 real")
	private Double preco;
	
	@NotNull(message = "Campo obrigatório")
	private Integer idCategoria;
	
	private Integer idLoja;
}
