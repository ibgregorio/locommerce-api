package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ItemDesejoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Campo obrigatório")
	private Long idLista;
	
	@NotNull(message = "Campo obrigatório")
	private Long idProduto;
}
