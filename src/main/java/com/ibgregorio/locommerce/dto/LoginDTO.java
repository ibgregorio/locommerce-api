package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Campo obrigatório")
	@Email(message = "E-mail inválido")
	private String email;
	
	@NotBlank(message = "Campo obrigatório")
	private String senha;
}
