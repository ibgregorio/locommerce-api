package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CNPJ;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LojaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotEmpty(message = "Campo obrigatório")
	@Length(max = 200, message = "O nome fantasia da loja deve ter no máx. 200 caracteres")
	private String nomeFantasia;
	
	@NotEmpty(message = "Campo obrigatório")
	@CNPJ(message = "CNPJ inválido")
	private String cnpj;
	
	@Length(max = 5000, message = "A descrição da loja deve ter no máx. 5000 caracteres")
	private String descricao;
	
	private String situacao;
	
	@Length(max = 5000, message = "A justificativa da reprovação deve ter no max 5000 caracteres")
	private String justificativaReprovacao;
	
	@NotNull(message = "Campo obrigatório")
	private Integer idCategoria;
}
