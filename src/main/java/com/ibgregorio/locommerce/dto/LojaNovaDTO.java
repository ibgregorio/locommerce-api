package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CNPJ;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LojaNovaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotEmpty(message = "Campo obrigatório")
	@Length(max = 200, message = "O nome fantasia da loja deve ter no máx. 200 caracteres")
	private String nomeFantasia;
	
	@NotEmpty(message = "Campo obrigatório")
	@CNPJ(message = "CNPJ inválido")
	private String cnpj;
	
	@NotNull(message = "Campo obrigatório")
	private Integer idCategoria;
	
	@Valid
	@NotNull(message = "Endereço da loja é obrigatório")
	private EnderecoDTO endereco;
	
	private Integer idProprietario;
}
