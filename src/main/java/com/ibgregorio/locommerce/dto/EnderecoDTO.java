package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;

	@NotEmpty(message = "Campo obrigatório")
	private String logradouro;
	
	@NotEmpty(message = "Campo obrigatório")
	private String numero;
	
	private String complemento;
	
	@NotEmpty(message = "Campo obrigatório")
	private String bairro;
	
	@NotEmpty(message = "Campo obrigatório")
	private String cep;
	
	@NotNull(message = "Campo obrigatório")
	private Integer idCidade;
	
	private String principal;
	
	private Long idUsuario;
	
	private Long idLoja;
	
}
