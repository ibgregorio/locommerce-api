package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AvaliacaoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	@NotNull(message = "Campo obrigatório")
	@Min(value = 1, message = "A nota mínima é de 1 estrela")
	@Max(value = 5, message = "A nota máxima é de 5 estrelas")
	private Integer nota;
	
	private Long idLojaAvaliada;
	
	private Long idUsuarioAvaliacao;

}
