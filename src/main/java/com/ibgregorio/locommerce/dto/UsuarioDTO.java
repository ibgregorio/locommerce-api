package com.ibgregorio.locommerce.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotEmpty(message = "Campo obrigatório")
	@Length(min = 5, max = 120, message = "O nome deve ter entre 5 a 120 caracteres")
	private String nome;
	
	@NotEmpty(message = "Campo obrigatório")
	@Email(message = "O e-mail informado é inválido")
	private String email;
	
	private String senha;
	
	@NotEmpty(message = "Campo obrigatório")
	@CPF(message = "CPF inválido")
	private String cpf;
	
	private String numeTel;
	
	private String numeCelular;
	
}
