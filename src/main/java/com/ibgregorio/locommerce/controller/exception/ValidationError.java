package com.ibgregorio.locommerce.controller.exception;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class ValidationError extends StandardError {

	private static final long serialVersionUID = 1L;
	
	@Getter
	private List<ValidationFieldMessage> errors = new ArrayList<>();

	public ValidationError(Integer status, String error, String message, String path, Long timestamp) {
		super(status, error, message, path, timestamp);
	}
	
	public void addValidationErrorMsg(String field, String msg) {
		errors.add(new ValidationFieldMessage(field, msg));
	}
	
}
