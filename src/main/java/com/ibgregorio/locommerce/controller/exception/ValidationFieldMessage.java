package com.ibgregorio.locommerce.controller.exception;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ValidationFieldMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private String field;
	
	private String msg;
}
