package com.ibgregorio.locommerce.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.controller.util.URL;
import com.ibgregorio.locommerce.dto.LojaDTO;
import com.ibgregorio.locommerce.dto.LojaNovaDTO;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.entity.enums.StatusEnum;
import com.ibgregorio.locommerce.mapper.LojaMapper;
import com.ibgregorio.locommerce.services.LojaService;

@RestController
@RequestMapping(value = "/lojas")
public class LojaController {
	
	@Autowired
	private LojaService service;
	
	@Autowired
	private LojaMapper mapper;
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Loja> findLoja(@PathVariable Long id) {
		Loja loja = service.findLoja(id);
		
		return ResponseEntity.ok().body(loja);
	}
	
	@GetMapping(value = "/produto/{id}")
	public ResponseEntity<Loja> findLojaByProduto(@PathVariable Long id) {
		Loja loja = service.findLojaByIdProduto(id);
		
		return ResponseEntity.ok().body(loja);
	}
	
	@GetMapping
	public ResponseEntity<List<LojaDTO>> searchLojas(
			@RequestParam(value = "nome", defaultValue = "") String filtroNome, 
			@RequestParam(value = "categoria", defaultValue = "") String idCategoria,
			@RequestParam(value = "status", defaultValue = "") String status) {
		
		String nomeDecoded = URL.decodeParam(filtroNome);
		Integer idCat = URL.convertParamToInteger(idCategoria);
		 
		StatusEnum situacao = null;
		if (status != null && !status.isEmpty()) {
			situacao = StatusEnum.toEnum(status);
		}
		
		List<Loja> list = service.searchLojas(nomeDecoded, idCat, situacao);
		List<LojaDTO> listDto = list.stream().map(obj -> mapper.lojaEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
	
	@PostMapping
	public ResponseEntity<Void> criarLoja(@Valid @RequestBody LojaNovaDTO lojaNovaDto) {
		Loja loja = mapper.lojaNovaDtoToEntity(lojaNovaDto);
		loja = service.criarLoja(loja);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(loja.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN') or hasAnyRole('VENDEDOR')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> update(@Valid @RequestBody LojaDTO lojaDto, @PathVariable Long id) {
		Loja loja = mapper.lojaDtoToEntity(lojaDto);
		loja.setId(id);
		service.update(loja);
		
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@PutMapping(value = "/aprovar/{id}")
	public ResponseEntity<Void> aprovarLoja(@PathVariable Long id) {
		service.aprovarLoja(id);
		
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@PutMapping(value = "/reprovar/{id}")
	public ResponseEntity<Void> reprovarLoja(@Valid @RequestBody LojaDTO lojaDto, @PathVariable Long id) {
		Loja loja = mapper.lojaDtoToEntity(lojaDto);
		loja.setId(id);
		service.reprovarLoja(loja);
		
		return ResponseEntity.noContent().build();
	}
}
