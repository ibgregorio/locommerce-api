package com.ibgregorio.locommerce.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.controller.util.URL;
import com.ibgregorio.locommerce.dto.ProdutoDTO;
import com.ibgregorio.locommerce.entity.Produto;
import com.ibgregorio.locommerce.mapper.ProdutoMapper;
import com.ibgregorio.locommerce.services.ProdutoService;

@RestController
@RequestMapping(value = "/produtos")
public class ProdutoController {

	@Autowired
	private ProdutoService service;
	
	@Autowired
	private ProdutoMapper mapper;
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Produto> findProduto(@PathVariable Long id) {
		Produto produto = service.findProduto(id);
		
		return ResponseEntity.ok().body(produto);
	}
	
	@GetMapping
	public ResponseEntity<List<ProdutoDTO>> searchLojas(
			@RequestParam(value = "nome", defaultValue = "") String filtroNome, 
			@RequestParam(value = "categoria", defaultValue = "") String idCategoria) {
		
		String nomeDecoded = URL.decodeParam(filtroNome);
		Integer idCat = URL.convertParamToInteger(idCategoria);
		
		List<Produto> list = service.searchProdutos(nomeDecoded, idCat);
		List<ProdutoDTO> listDto = list.stream().map(obj -> mapper.produtoEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
	
	@GetMapping(value = "/loja/{idLoja}")
	public ResponseEntity<List<ProdutoDTO>> loadProdutosFromLoja(@PathVariable Long idLoja) {
		List<Produto> list = service.loadProdutosFromLoja(idLoja);
		List<ProdutoDTO> listDto = list.stream().map(obj -> mapper.produtoEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
	
	@PreAuthorize("hasAnyRole('VENDEDOR')")
	@PostMapping
	public ResponseEntity<Void> criarProduto(@Valid @RequestBody ProdutoDTO produtoDto) {
		Produto produto = mapper.produtoDtoToEntity(produtoDto);
		produto = service.criarProduto(produto);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(produto.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('VENDEDOR')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> update(@Valid @RequestBody ProdutoDTO produtoDto, @PathVariable Long id) {
		Produto produto = mapper.produtoDtoToEntity(produtoDto);
		produto.setId(id);
		service.update(produto);
		
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('VENDEDOR')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.deleteProduto(id);
		
		return ResponseEntity.noContent().build();
	}
}
