package com.ibgregorio.locommerce.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.dto.ItemDesejoDTO;
import com.ibgregorio.locommerce.entity.ItemDesejo;
import com.ibgregorio.locommerce.services.ItemDesejoService;

@RestController
@RequestMapping(value = "/listaDesejos/itens")
public class ItemDesejoController {

	@Autowired
	private ItemDesejoService service;
	
	@PostMapping
	public ResponseEntity<Void> adicionarProduto(@Valid @RequestBody ItemDesejoDTO item) {
		ItemDesejo itemAdicionado = service.adicionarProduto(item);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(itemAdicionado.getProduto().getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@DeleteMapping(value = "/{idItem}/selectedList/{idList}")
	public ResponseEntity<Void> removerProduto(@PathVariable Long idItem, @PathVariable Long idList) {
		ItemDesejoDTO dto = new ItemDesejoDTO();
		dto.setIdLista(idList);
		dto.setIdProduto(idItem);		
		
		service.removerProduto(dto);
		
		return ResponseEntity.noContent().build();
	}
}
