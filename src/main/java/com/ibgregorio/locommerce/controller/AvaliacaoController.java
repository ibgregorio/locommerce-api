package com.ibgregorio.locommerce.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.dto.AvaliacaoDTO;
import com.ibgregorio.locommerce.entity.Avaliacao;
import com.ibgregorio.locommerce.mapper.AvaliacaoMapper;
import com.ibgregorio.locommerce.services.AvaliacaoService;

@RestController
@RequestMapping(value = "/avaliacoes")
public class AvaliacaoController {

	@Autowired
	private AvaliacaoService service;
	
	@Autowired
	private AvaliacaoMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<AvaliacaoDTO>> findAll() {
		List<Avaliacao> list = service.findAllAvaliacoes();
		List<AvaliacaoDTO> listDto = list.stream().map(obj -> mapper.avaliacaoEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
	
	@PreAuthorize("hasAnyRole('CLIENTE')")
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody AvaliacaoDTO avaliacaoDto) {
		Avaliacao avaliacao = mapper.avaliacaoDtoToEntity(avaliacaoDto);
		avaliacao = service.save(avaliacao);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(avaliacao.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('CLIENTE')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> update(@Valid @RequestBody AvaliacaoDTO avaliacaoDto, @PathVariable Long id) {
		Avaliacao avaliacao = mapper.avaliacaoDtoToEntity(avaliacaoDto);
		avaliacao.setId(id);
		service.editarAvaliacao(avaliacao);
		
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('CLIENTE')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.removerAvaliacao(id);
		
		return ResponseEntity.noContent().build();
	}
	
}
