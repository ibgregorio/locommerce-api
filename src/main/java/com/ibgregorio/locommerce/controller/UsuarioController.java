package com.ibgregorio.locommerce.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.dto.UsuarioDTO;
import com.ibgregorio.locommerce.dto.UsuarioNovoDTO;
import com.ibgregorio.locommerce.entity.Usuario;
import com.ibgregorio.locommerce.mapper.UsuarioMapper;
import com.ibgregorio.locommerce.services.UsuarioService;

@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService service;
	
	@Autowired
	private UsuarioMapper mapper;

	@GetMapping(value = "/{id}")
	public ResponseEntity<Usuario> findUsuario(@PathVariable Long id) {
		Usuario usuario = service.findUsuario(id);
		
		return ResponseEntity.ok().body(usuario);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<UsuarioDTO>> findAll() {
		List<Usuario> list = service.findAllUsuarios();
		List<UsuarioDTO> listDto = list.stream().map(obj -> mapper.usuarioEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
	
	@GetMapping(value="/email")
	public ResponseEntity<Usuario> find(@RequestParam(value="value") String email) {
		Usuario usuario = service.findByEmail(email);
		
		return ResponseEntity.ok().body(usuario);
	}
	
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody UsuarioNovoDTO usuarioNovoDto) {
		Usuario usuario = service.generateNewUsuario(usuarioNovoDto);
		usuario = service.cadastrar(usuario);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(usuario.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> update(@Valid @RequestBody UsuarioDTO usuarioDto, @PathVariable Long id) {
		Usuario usuario = mapper.usuarioDtoToEntity(usuarioDto);
		usuario.setId(id);
		service.update(usuario);
		
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.delete(id);
		
		return ResponseEntity.noContent().build();
	}
}
