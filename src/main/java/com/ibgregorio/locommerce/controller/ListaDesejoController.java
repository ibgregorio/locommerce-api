package com.ibgregorio.locommerce.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.dto.ListaDesejoDTO;
import com.ibgregorio.locommerce.entity.ListaDesejo;
import com.ibgregorio.locommerce.mapper.ListaDesejoMapper;
import com.ibgregorio.locommerce.services.ListaDesejoService;

@RestController
@RequestMapping(value = "/listaDesejos")
public class ListaDesejoController {

	@Autowired
	private ListaDesejoService service;
	
	@Autowired
	private ListaDesejoMapper mapper;
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<ListaDesejo> find(@PathVariable Long id) {
		ListaDesejo pedido = service.findListaDesejo(id);
		
		return ResponseEntity.ok().body(pedido);
	}
	
	@GetMapping
	public ResponseEntity<List<ListaDesejo>> loadListaDesejos() {
		List<ListaDesejo> list = service.loadListasUsuario();
		
		return ResponseEntity.ok().body(list);
	}
	
	@PostMapping
	public ResponseEntity<Void> criarListaDesejo(@Valid @RequestBody ListaDesejoDTO listaDto) {
		ListaDesejo listaDesejo = mapper.listaDesejoDtoToEntity(listaDto);
		listaDesejo = service.criarLista(listaDesejo);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(listaDesejo.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> atualizarLista(@Valid @RequestBody ListaDesejoDTO listaDto, @PathVariable Long id) {
		listaDto.setId(id);
		service.editarLista(listaDto);
		
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.delete(id);
		
		return ResponseEntity.noContent().build();
	}
}
