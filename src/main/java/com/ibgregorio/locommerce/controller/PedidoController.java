package com.ibgregorio.locommerce.controller;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.dto.PedidoDTO;
import com.ibgregorio.locommerce.entity.Pedido;
import com.ibgregorio.locommerce.services.PedidoService;

@RestController
@RequestMapping(value = "/pedidos")
public class PedidoController {

	@Autowired
	private PedidoService service;
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Pedido> find(@PathVariable Long id) {
		Pedido pedido = service.findPedido(id);
		
		return ResponseEntity.ok().body(pedido);
	}
	
	@GetMapping
	public ResponseEntity<List<Pedido>> loadPedidosComprador() {
		List<Pedido> list = service.loadPedidosComprador();
		
		return ResponseEntity.ok().body(list);
	}
	
	@GetMapping(value = "/loja")
	@PreAuthorize("hasAnyRole('VENDEDOR')")
	public ResponseEntity<List<Pedido>> loadPedidosLoja() {
		List<Pedido> list = service.loadPedidosVendedor();
		
		return ResponseEntity.ok().body(list);
	}
	
	@PostMapping
	public ResponseEntity<Void> finalizarPedido(@Valid @RequestBody Pedido pedido, HttpServletResponse response) {
		pedido = service.finalizarPedido(pedido);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(pedido.getId()).toUri();
		
		response.addHeader("access-control-expose-headers", "Location");
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(value = "/{id}")
	@PreAuthorize("hasAnyRole('VENDEDOR')")
	public ResponseEntity<Void> atualizarStatus(@Valid @RequestBody PedidoDTO pedidoDto, @PathVariable Long id) {
		pedidoDto.setId(id);
		service.atualizarStatus(pedidoDto);
		
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> cancelarPedido(@PathVariable Long id) {
		service.cancelarPedido(id);
		
		return ResponseEntity.noContent().build();
	}
}
