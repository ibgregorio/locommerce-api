package com.ibgregorio.locommerce.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibgregorio.locommerce.dto.CidadeDTO;
import com.ibgregorio.locommerce.dto.EstadoDTO;
import com.ibgregorio.locommerce.entity.Cidade;
import com.ibgregorio.locommerce.entity.Estado;
import com.ibgregorio.locommerce.mapper.CidadeMapper;
import com.ibgregorio.locommerce.mapper.EstadoMapper;
import com.ibgregorio.locommerce.services.CidadeService;
import com.ibgregorio.locommerce.services.EstadoService;

@RestController
@RequestMapping(value = "/estados")
public class EstadoController {

	@Autowired
	private EstadoService service;
	
	@Autowired
	private CidadeService cidadeService;
	
	@Autowired
	private EstadoMapper mapper;
	
	@Autowired
	private CidadeMapper cidadeMapper;
	
	@GetMapping
	public ResponseEntity<List<EstadoDTO>> findAll() {
		List<Estado> list = service.findAll();
		List<EstadoDTO> listDto = list.stream().map(obj -> mapper.estadoEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
	
	@GetMapping(value = "/{idEstado}/cidades")
	public ResponseEntity<List<CidadeDTO>> findCidades(@PathVariable Integer idEstado) {
		List<Cidade> list = cidadeService.findCidadesByEstado(idEstado);
		List<CidadeDTO> listDto = list.stream().map(obj -> cidadeMapper.cidadeEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
}
