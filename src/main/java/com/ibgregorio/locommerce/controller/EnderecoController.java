package com.ibgregorio.locommerce.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.locommerce.dto.EnderecoDTO;
import com.ibgregorio.locommerce.entity.Endereco;
import com.ibgregorio.locommerce.mapper.EnderecoMapper;
import com.ibgregorio.locommerce.services.EnderecoService;

@RestController
@RequestMapping(value = "/enderecos")
public class EnderecoController {
	
	@Autowired
	private EnderecoService service;
	
	@Autowired
	private EnderecoMapper mapper;

	@GetMapping(value = "/{id}")
	public ResponseEntity<Endereco> findEndereco(@PathVariable Long id) {
		Endereco endereco = service.findEndereco(id);
		
		return ResponseEntity.ok().body(endereco);
	}
	
	@GetMapping
	public ResponseEntity<List<EnderecoDTO>> findAll() {
		List<Endereco> list = service.findEnderecoByUsuario();
		List<EnderecoDTO> listDto = list.stream().map(obj -> mapper.enderecoEntityToDto(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listDto);
	}
	
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody EnderecoDTO enderecoDto) {
		Endereco endereco = mapper.enderecoDtoToEntity(enderecoDto);
		endereco = service.adicionarEndereco(endereco);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(endereco.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> update(@Valid @RequestBody EnderecoDTO enderecoDto, @PathVariable Long id) {
		Endereco endereco = mapper.enderecoDtoToEntity(enderecoDto);
		endereco.setId(id);
		service.update(endereco);
		
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.delete(id);
		
		return ResponseEntity.noContent().build();
	}
}
