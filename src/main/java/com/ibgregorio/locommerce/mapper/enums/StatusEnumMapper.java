package com.ibgregorio.locommerce.mapper.enums;

import org.mapstruct.Mapper;

import com.ibgregorio.locommerce.entity.enums.StatusEnum;

@Mapper(componentModel = "spring")
public interface StatusEnumMapper {
	
	default StatusEnum toEnum(String value) {
        if (value == null) {
            return null;
        }
        
        return StatusEnum.toEnum(value);
    }

	default String toString(StatusEnum enume) {
		if (enume == null) {
			return null;
		}
		
        return enume.getId();
    }
}
