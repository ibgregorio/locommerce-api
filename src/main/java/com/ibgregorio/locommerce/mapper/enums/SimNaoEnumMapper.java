package com.ibgregorio.locommerce.mapper.enums;

import org.mapstruct.Mapper;

import com.ibgregorio.locommerce.entity.enums.SimNaoEnum;

@Mapper(componentModel = "spring")
public interface SimNaoEnumMapper {
	
	default SimNaoEnum toEnum(String value) {
        if (value == null) {
            return null;
        }
        
        return SimNaoEnum.toEnum(value);
    }

	default String toString(SimNaoEnum enume) {
		if (enume == null) {
			return null;
		}
		
        return enume.getId();
    }
}
