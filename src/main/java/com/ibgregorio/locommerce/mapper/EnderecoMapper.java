package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.ibgregorio.locommerce.dto.EnderecoDTO;
import com.ibgregorio.locommerce.entity.Endereco;
import com.ibgregorio.locommerce.mapper.annotation.DefaultMethodMapping;
import com.ibgregorio.locommerce.mapper.enums.SimNaoEnumMapper;
import com.ibgregorio.locommerce.services.CidadeService;
import com.ibgregorio.locommerce.services.UsuarioService;

@Mapper(componentModel = "spring", uses = { CidadeService.class, UsuarioService.class, SimNaoEnumMapper.class })
public interface EnderecoMapper {

	@Mapping(source = "idCidade", target = "cidade")
	@Mapping(source = "idUsuario", target = "usuario", qualifiedBy = { DefaultMethodMapping.class })
	Endereco enderecoDtoToEntity(final EnderecoDTO enderecoDto);
	
	@Mapping(source = "cidade.id", target = "idCidade")
	@Mapping(source = "usuario.id", target = "idUsuario")
	EnderecoDTO enderecoEntityToDto(final Endereco endereco);
}
