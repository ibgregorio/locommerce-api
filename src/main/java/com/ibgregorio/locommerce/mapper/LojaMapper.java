package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.ibgregorio.locommerce.dto.LojaDTO;
import com.ibgregorio.locommerce.dto.LojaNovaDTO;
import com.ibgregorio.locommerce.entity.Loja;
import com.ibgregorio.locommerce.mapper.annotation.DefaultMethodMapping;
import com.ibgregorio.locommerce.mapper.enums.SimNaoEnumMapper;
import com.ibgregorio.locommerce.mapper.enums.StatusEnumMapper;
import com.ibgregorio.locommerce.services.CategoriaService;
import com.ibgregorio.locommerce.services.CidadeService;
import com.ibgregorio.locommerce.services.UsuarioService;

@Mapper(componentModel = "spring", uses = { EnderecoMapper.class, UsuarioService.class, CategoriaService.class, CidadeService.class, StatusEnumMapper.class, SimNaoEnumMapper.class })
public interface LojaMapper {

	@Mapping(source = "idCategoria", target = "categoria")
	@Mapping(target = "endereco", ignore = true)
	@Mapping(target = "proprietario", ignore = true)
	Loja lojaDtoToEntity(final LojaDTO lojaDto);
	
	@Mapping(target = "situacao", ignore = true)
	@Mapping(target = "justificativaReprovacao", ignore = true)
	@Mapping(target = "endereco.id", ignore = true)
	@Mapping(target = "endereco.loja", ignore = true)
	@Mapping(target = "endereco.usuario", ignore = true)
	@Mapping(source = "idCategoria", target = "categoria")
	@Mapping(source = "idProprietario", target = "proprietario", qualifiedBy = { DefaultMethodMapping.class })
	@Mapping(source = "endereco.idCidade", target = "endereco.cidade")
	Loja lojaNovaDtoToEntity(final LojaNovaDTO lojaNovaDto);
	
	@Mapping(source = "categoria.id", target = "idCategoria")
	LojaDTO lojaEntityToDto(final Loja loja);
}