package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.ibgregorio.locommerce.dto.ProdutoDTO;
import com.ibgregorio.locommerce.entity.Produto;
import com.ibgregorio.locommerce.mapper.annotation.DefaultMethodMapping;
import com.ibgregorio.locommerce.services.CategoriaService;
import com.ibgregorio.locommerce.services.LojaService;

@Mapper(componentModel = "spring", uses = { CategoriaService.class, LojaService.class })
public interface ProdutoMapper {

	@Mapping(target = "dtAtualizacaoPreco", ignore = true)
	@Mapping(source = "idCategoria", target = "categoria")
	@Mapping(source = "idLoja", target = "loja", qualifiedBy = { DefaultMethodMapping.class })
	Produto produtoDtoToEntity(final ProdutoDTO produtoDto);
	
	@Mapping(source = "categoria.id", target = "idCategoria")
	@Mapping(source = "loja.id", target = "idLoja")
	ProdutoDTO produtoEntityToDto(final Produto produto);
}
