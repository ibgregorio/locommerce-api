package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.ibgregorio.locommerce.dto.UsuarioDTO;
import com.ibgregorio.locommerce.entity.Usuario;

@Mapper(componentModel = "spring")
public interface UsuarioMapper {

	Usuario usuarioDtoToEntity(final UsuarioDTO usuarioDto);
	
	@Mapping(target = "senha", ignore = true)
	UsuarioDTO usuarioEntityToDto(final Usuario usuario);
}
