package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.ibgregorio.locommerce.dto.CategoriaDTO;
import com.ibgregorio.locommerce.entity.Categoria;

@Mapper(componentModel = "spring")
public interface CategoriaMapper {

	@Mapping(target = "lojas", ignore = true)
	Categoria categoriaDtoToEntity(final CategoriaDTO categoriaDto);
	
	CategoriaDTO categoriaEntityToDto(final Categoria categoria);
}
