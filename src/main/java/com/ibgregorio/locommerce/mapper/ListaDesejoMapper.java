package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;

import com.ibgregorio.locommerce.dto.ListaDesejoDTO;
import com.ibgregorio.locommerce.entity.ListaDesejo;
import com.ibgregorio.locommerce.services.UsuarioService;

@Mapper(componentModel = "spring", uses = { UsuarioService.class })
public interface ListaDesejoMapper {
	
	ListaDesejo listaDesejoDtoToEntity(final ListaDesejoDTO listaDesejoDTO);
	
	ListaDesejoDTO listaDesejoEntityToDto(final ListaDesejo listaDesejo);
}
