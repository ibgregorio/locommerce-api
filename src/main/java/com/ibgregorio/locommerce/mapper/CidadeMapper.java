package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;

import com.ibgregorio.locommerce.dto.CidadeDTO;
import com.ibgregorio.locommerce.entity.Cidade;

@Mapper(componentModel = "spring")
public interface CidadeMapper {
	
	CidadeDTO cidadeEntityToDto(final Cidade cidade);
}
