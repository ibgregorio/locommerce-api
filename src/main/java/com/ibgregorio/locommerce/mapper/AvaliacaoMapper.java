package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.ibgregorio.locommerce.dto.AvaliacaoDTO;
import com.ibgregorio.locommerce.entity.Avaliacao;
import com.ibgregorio.locommerce.mapper.annotation.DefaultMethodMapping;
import com.ibgregorio.locommerce.services.LojaService;
import com.ibgregorio.locommerce.services.UsuarioService;

@Mapper(componentModel = "spring", uses = { UsuarioService.class, LojaService.class })
public interface AvaliacaoMapper {

	@Mapping(source = "idLojaAvaliada", target = "lojaAvaliada", qualifiedBy = { DefaultMethodMapping.class })
	@Mapping(source = "idUsuarioAvaliacao", target = "usuarioAvaliacao", qualifiedBy = { DefaultMethodMapping.class })
	@Mapping(target = "dtAvaliacao", ignore = true)
	Avaliacao avaliacaoDtoToEntity(final AvaliacaoDTO avaliacaoDto);
	
	@Mapping(source = "lojaAvaliada.id", target = "idLojaAvaliada")
	@Mapping(source = "usuarioAvaliacao.id", target = "idUsuarioAvaliacao")
	AvaliacaoDTO avaliacaoEntityToDto(final Avaliacao avaliacao);
}
