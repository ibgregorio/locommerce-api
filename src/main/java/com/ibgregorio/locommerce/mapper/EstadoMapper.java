package com.ibgregorio.locommerce.mapper;

import org.mapstruct.Mapper;

import com.ibgregorio.locommerce.dto.EstadoDTO;
import com.ibgregorio.locommerce.entity.Estado;

@Mapper(componentModel = "spring")
public interface EstadoMapper {

	EstadoDTO estadoEntityToDto(final Estado estado);
}
