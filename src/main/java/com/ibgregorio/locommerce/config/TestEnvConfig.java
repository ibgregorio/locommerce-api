package com.ibgregorio.locommerce.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.ibgregorio.locommerce.services.email.EmailMockService;
import com.ibgregorio.locommerce.services.email.EmailService;
import com.ibgregorio.locommerce.services.mock.MockDataService;

@Configuration
@Profile("test")
public class TestEnvConfig {

	@Autowired
	private MockDataService mockDataService;

	@Bean
	public boolean instantiateDatabase() throws Exception {
		mockDataService.initDatabase();

		return true;
	}
	
	@Bean
	public EmailService emailService() {
		return new EmailMockService();
	}
}
