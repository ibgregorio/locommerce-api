package com.ibgregorio.locommerce.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.ibgregorio.locommerce.services.email.EmailService;
import com.ibgregorio.locommerce.services.email.SmtpEmailService;

@Configuration
@Profile("dev")
public class DevEnvConfig {
	
	@Bean
	public EmailService emailService() {
		return new SmtpEmailService();
	}
}
